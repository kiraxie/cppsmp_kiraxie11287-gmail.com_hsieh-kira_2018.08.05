/**************************************************************************************************
 *  ipcPipe.cpp
 *************************************************************************************************/
#include "ipcPipe.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>


using namespace std;

/**************************************************************************************************
 * Private Definition
 *************************************************************************************************/
#define PIPE_FPATH                          "/tmp/ipcPipe"

/**************************************************************************************************
 * PRIVATE FUNCTIONS
 *************************************************************************************************/
static int fileExist(const char *filename)
{
    struct stat         buffer = {0};

    return (stat(filename, &buffer) == 0);
}

/**************************************************************************************************
 * Class
 *************************************************************************************************/
ipcPipe::ipcPipe(int flag)
{
    if(!fileExist(PIPE_FPATH)) {
        if(mkfifo(PIPE_FPATH, 0644) < 0) {
            _pipeFd = -1;
        } else {
            _pipeFd = open(PIPE_FPATH, flag);
        }
    } else {
        _pipeFd = open(PIPE_FPATH, flag);
    }
}

ipcPipe::~ipcPipe()
{
    if(_pipeFd >= 0) {
        close(_pipeFd);
        unlink(PIPE_FPATH);
    }
}

ipcPacketCmd ipcPipe::ipcRecv(std::vector<unsigned int> *_v)
{
    ipcPacketHdr        hdr;
    int                 arrSize = 0;
    int                 val = 0;

    _read(&hdr, sizeof(ipcPacketHdr));

    if(hdr.type == IPC_PACKET_CMD_DATA) {
        arrSize = hdr.len / sizeof(int);
        _v->resize(arrSize);
        _v->clear();

        for(int i = 0; i < arrSize; ++i) {
            _read(&val, sizeof(int));
            _v->push_back(val);
        }
    }

    return hdr.type;
}

void ipcPipe::ipcSend(ipcPacketCmd type, std::vector<unsigned int> *_v)
{
    ipcPacketHdr        hdr;
    int                 val = 0;

    memset(&hdr, 0, sizeof(ipcPacketHdr));

    hdr.type = type;
    if(_v) {
        hdr.len = _v->size() * sizeof(int);
    }

    _write(&hdr, sizeof(ipcPacketHdr));

    if(type == IPC_PACKET_CMD_DATA) {
        for(unsigned int i = 0; i < _v->size(); ++i) {
            val = _v->at(i);
            _write(&val, sizeof(int));
        }
    }
}
