/**************************************************************************************************
 *  mainParent.cpp
 *************************************************************************************************/
#include <iostream>
#include <string>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "global.h"
#include "ipcShm.h"
#include "ipcPipe.h"

using namespace std;

/**************************************************************************************************
 * PRIVATE DEFINITION
 *************************************************************************************************/
#define RAND_MIN        (50)

/**************************************************************************************************
 * PRIVATE FUNCTIONS
 *************************************************************************************************/
static int getInteger(void)
{
    string              str;

    cin >> str;

    for(unsigned int i = 0; i < str.size(); i++) {
        if(str[i] < '0' || str[i] > '9') {
            return -1;
        }
    }

    return stoi(str, 0, 10);
}

static int mainParent(void)
{
    std::vector<unsigned int>   valArr;
    ipcPipe                     *pipe = NULL;
    ipcShm                      *shm = new ipcShm();

    cout << "[" << __FUNCTION__ << "] Main Process Started" << endl;

    if(system("./childA &") < 0) {
        cout << "[" << __FUNCTION__ << "] childA initial failed!" << endl;
        delete shm;
        exit(-1);
    }
    pipe = new ipcPipe(O_WRONLY);

    if(system("./childB &") < 0) {
        cout << "[" << __FUNCTION__ << "] childB initial failed!" << endl;
        delete shm;
        exit(-1);
    }

    pipe->master();
    shm->master();

    pipe->take(IPC_SEM_TASK_A);
    shm->take(IPC_SEM_TASK_B);

    srand(time(NULL));
    cout << endl << endl;


    do {
        int                 n = 0;

        cout << "[" << __FUNCTION__ << "] Enter a positive integer or 0 to exit: ";
        n = getInteger();

        if(n < 0) {
            cout << "[" << __FUNCTION__ << "] Can only accept positive integers, please try again." << endl;
            continue;
        } else if(n == 0) {
            cout << "[" << __FUNCTION__ << "] Process Waits" << endl;
            pipe->ipcSend(IPC_PACKET_CMD_EXIT, NULL);
            shm->ipcSend(IPC_PACKET_CMD_EXIT, NULL);

            pipe->take(IPC_SEM_TASK_A);
            shm->take(IPC_SEM_TASK_B);
            cout << "[" << __FUNCTION__ << "] Process Exits" << endl;
            break;
        }

        valArr.resize(n);
        valArr.clear();

        cout << "[" << __FUNCTION__ << "] Generating "<< n << " random integers: ";
        for(int i = 0; i < n; ++i) {
            valArr.push_back(rand() % RAND_MIN);
            cout <<  (valArr.at(i) + RAND_NUM_BASE) << " " ;
        }
        cout << endl;

        pipe->ipcSend(IPC_PACKET_CMD_DATA, &valArr);
        shm->ipcSend(IPC_PACKET_CMD_DATA, &valArr);

        //  Wait child done
        shm->take(IPC_SEM_TASK_B);

        cout << endl << endl;
    } while(1);

    delete pipe;
    delete shm;

    return 0;
}

/**************************************************************************************************
 * MAIN FUNCTIONS
 *************************************************************************************************/
int main(int argc, char const *argv[])
{
    return mainParent();
}
