/**************************************************************************************************
 *  task.cpp
 *************************************************************************************************/
#include <iostream>
#include <algorithm>

#include "task.h"

using namespace std;

/**************************************************************************************************
 * Class
 *************************************************************************************************/
void task::printData_(std::vector<unsigned int> *_v)
{
    for(unsigned int i = 0; i < _v->size(); i++) {
        cout << (_v->at(i) + RAND_NUM_BASE) << " ";
    }
    cout << endl;
}

void task::printSortData_(std::vector<unsigned int> *_v)
{
    sort(_v->begin(),_v->end());
    for(unsigned int i = 0; i < _v->size(); i++) {
        cout << (_v->at(i) + RAND_NUM_BASE) << " ";
    }
    cout << endl;
}

/*  Task A  */
ipcPacketCmd taskA::recvData(void)
{
    return ipcRecv(&_v);
}

void taskA::printData(void)
{
    printData_(&_v);
}

void taskA::printSortData(void)
{
    printSortData_(&_v);
}

double taskA::calculateMedian(void)
{
    double              median = 0;
    unsigned int        len = _v.size();

    if(len % 2) {
        median = (double)_v.at(len / 2);
    } else {
        median = (double)(_v.at(len / 2) + _v.at((len / 2) - 1)) / 2;
    }

    return median + RAND_NUM_BASE;
}

/*  Task B  */
ipcPacketCmd taskB::recvData(void)
{
    return ipcRecv(&_v);
}

void taskB::printData(void)
{
    printData_(&_v);
}

void taskB::printSortData(void)
{
    printSortData_(&_v);
}

double taskB::calculateGeometricMean(void)
{
    int                 sum = 0;

    for(unsigned int i = 0; i < _v.size(); i++) {
        sum += _v.at(i);
    }

    return (((double)sum / _v.size()) + RAND_NUM_BASE);
}
