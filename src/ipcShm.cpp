/**************************************************************************************************
 *  ipcShm.cpp
 *************************************************************************************************/
#include <iostream>

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#include "ipcShm.h"

using namespace std;

/**************************************************************************************************
 * Private Definition
 *************************************************************************************************/
#define SHM_KEY                             (0x3344)

/**************************************************************************************************
 * Class
 *************************************************************************************************/
ipcShm::ipcShm()
{
    if((_shmId = shmget(SHM_KEY, SHM_MAX_MEM_SIZE, IPC_CREAT | 0644)) >= 0) {
        if((_shmAddr = shmat(_shmId, NULL, 0)) == (void *) -1) {
            shmctl(_shmId, IPC_RMID, NULL);
            _shmId = -1;
        }
    }
}

ipcShm::~ipcShm()
{
    if(_shmId >= 0) {
        shmdt(_shmAddr);
        shmctl(_shmId, IPC_RMID, NULL);
    }
}

ipcPacketCmd ipcShm::ipcRecv(std::vector<unsigned int> *_v)
{
    ipcPacketHdr        hdr;
    unsigned int        arrSize = 0;
    unsigned int        count = 0;

    _readHdr(&hdr);

    if(hdr.type == IPC_PACKET_CMD_DATA) {
        arrSize = hdr.len / sizeof(int);
        count = 0;
        _v->resize(arrSize);
        _v->clear();

        while(count < arrSize) {
            int l = 0;

            if(arrSize - count > SHM_MAX_MEM_SIZE / sizeof(int)) {
                l = SHM_MAX_MEM_SIZE / sizeof(int);
            } else {
                l = arrSize - count;
            }

            _readData(_v, count, l);
            count += l;
        }
    }

    return hdr.type;
}

void ipcShm::ipcSend(ipcPacketCmd type, std::vector<unsigned int> *_v)
{
    ipcPacketHdr        hdr;
    unsigned int        offset = 0;
    int                 maxCount = SHM_MAX_MEM_SIZE / sizeof(int);

    memset(&hdr, 0, sizeof(ipcPacketHdr));

    hdr.type = type;
    if(_v) {
        hdr.len = _v->size() * sizeof(int);
    }

    _writeHdr(&hdr);

    if(type == IPC_PACKET_CMD_DATA) {
        while(_v->size() != offset) {
            int len = _v->size() - offset;

            if(len > maxCount) {
                len = maxCount;
            }

            _writeData(_v, offset, len);

            offset += len;
        }
    }
}
