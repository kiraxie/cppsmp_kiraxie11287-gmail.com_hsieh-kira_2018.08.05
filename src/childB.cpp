/**************************************************************************************************
 *  childB.cpp
 *************************************************************************************************/
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include "task.h"

using namespace std;

/**************************************************************************************************
 * PRIVATE DEFINITION
 *************************************************************************************************/
static int childB(void)
{
    taskB               *t = new taskB();
    int                 cmd = 0;

    cout << "[" << __FUNCTION__ << "] Child process started" << endl;

    t->give(IPC_SEM_TASK_B);

    while(1) {
        cmd = t->recvData();

        if(cmd == IPC_PACKET_CMD_EXIT) {
            break;
        }

        cout << "[" << __FUNCTION__ << "] Random Numbers Received From Shared Memory: ";
        t->printData();

        cout << "[" << __FUNCTION__ << "] Sorted Sequence: ";
        t->printSortData();

        cout << "[" << __FUNCTION__ << "] Geometric Mean: ";
        cout << setiosflags(ios::fixed) << setprecision(7) << t->calculateGeometricMean() << endl;

        t->give(IPC_SEM_TASK_B);
    }

    cout << "[" << __FUNCTION__ << "] Child process exits" << endl;
    t->give(IPC_SEM_TASK_B);

    return 0;
}


/**************************************************************************************************
 * Main Function
 *************************************************************************************************/
int main(int argc, char const *argv[])
{
    return childB();
}
