/**************************************************************************************************
 *  childA.cpp
 *************************************************************************************************/
#include <iostream>
#include <iomanip>
#include <unistd.h>
#include "task.h"

using namespace std;

/**************************************************************************************************
 * PRIVATE DEFINITION
 *************************************************************************************************/
static int childA(void)
{
    taskA               *t = new taskA();
    int                 cmd = 0;

    cout << "[" << __FUNCTION__ << "] Child process started" << endl;

    t->give(IPC_SEM_TASK_A);

    while(1) {
        cmd = t->recvData();

        if(cmd == IPC_PACKET_CMD_EXIT) {
            break;
        }

        cout << "[" << __FUNCTION__ << "] Random Numbers Received From Pipe: ";
        t->printData();

        cout << "[" << __FUNCTION__ << "] Median: ";
        cout << setiosflags(ios::fixed) << setprecision(7) << t->calculateMedian() << endl;
    }

    cout << "[" << __FUNCTION__ << "] Child process exits" << endl;
    t->give(IPC_SEM_TASK_A);

    return 0;
}


/**************************************************************************************************
 * Main Function
 *************************************************************************************************/
int main(int argc, char const *argv[])
{
    return childA();
}
