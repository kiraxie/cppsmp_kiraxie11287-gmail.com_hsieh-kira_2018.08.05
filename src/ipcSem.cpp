/**************************************************************************************************
 *  ipcSem.cpp
 *************************************************************************************************/
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <string.h>
#include <errno.h>

#include "ipcSem.h"

using namespace std;

/**************************************************************************************************
 * Private Definition
 *************************************************************************************************/
#define SEM_KEY                             (0x5566)

typedef union {
    int                 val;
    struct semid_ds     *buf;
    unsigned short      *array;
} semun;

/**************************************************************************************************
 * Class
 *************************************************************************************************/
ipcSem::ipcSem()
{
    semun               semArg;

    _master = false;
    if((_semID = semget(SEM_KEY, IPC_SEM_MAX, IPC_CREAT | IPC_EXCL | 0644)) >= 0) {
        for(int i = 0; i < IPC_SEM_MAX; ++i) {
            memset(&semArg, 0, sizeof(semun));
            semArg.val = 0;
            if(semctl(_semID, i, SETVAL, semArg) < 0) {
                for(int j = 0; j < i; ++j) {
                    semctl(_semID, j, IPC_RMID, semArg);
                }
                _semID = -1;
            }
        }

        //  Initial sem val
        give(IPC_SEM_MASTER);
    } else if(errno == EEXIST) {
        _semID = semget(SEM_KEY, 0, 0);
    }
}

ipcSem::~ipcSem()
{
    semun               semArg;

    if(_semID >= 0 && _master) {
        for(int i = 0; i < IPC_SEM_MAX; ++i) {
            semctl(_semID, i, IPC_RMID, semArg);
        }
    }
}

int ipcSem::take(unsigned short num)
{
    struct sembuf       op = {0};

    op.sem_num = num;
    op.sem_op = -1;
    op.sem_flg = SEM_UNDO;

    return semop(_semID, &op, 1);
}

int ipcSem::give(unsigned short num)
{
    struct sembuf       op = {0};

    op.sem_num = num;
    op.sem_op = 1;
    op.sem_flg = SEM_UNDO;

    return semop(_semID, &op, 1);
}

int ipcSem::watch(unsigned short num)
{
    semun               semArg;

    return semctl(_semID, num, GETVAL, semArg);
}

void ipcSem::master(void)
{
    _master = true;
}
