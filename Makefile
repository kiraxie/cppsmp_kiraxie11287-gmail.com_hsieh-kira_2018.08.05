CC          = g++
CURR_DIR    = $(PWD)
SRC_DIR     = $(CURR_DIR)/src
INC_DIR     = $(CURR_DIR)/inc
LIBS        = -lstdc++
FLAGS       = -Wall -Werror -std=c++11 $(LIBS) -I$(INC_DIR)

ALL_SRC     = $(wildcard src/*.cpp)
ALL_OBJ     = $(patsubst %.cpp, %.o, $(ALL_SRC))

TARGET      = mainParent childA childB

MAIN_OBJ    = $(addprefix $(SRC_DIR)/, mainParent.o ipcSem.o ipcPipe.o ipcShm.o)

TASKA_OBJ   =  $(addprefix $(SRC_DIR)/, ipcSem.o ipcPipe.o ipcShm.o task.o childA.o)

TASKB_OBJ   =  $(addprefix $(SRC_DIR)/, ipcSem.o ipcPipe.o ipcShm.o task.o childB.o)

%.o:%.cpp
	@$(CC) $(FLAGS) -c $< -o $@

mainParent: $(MAIN_OBJ)
	@echo -n "Build mainParent ... "
	@$(CC) $(MAIN_OBJ) $(FLAGS) -o mainParent
	@echo "Complete"

childA: $(TASKA_OBJ)
	@echo -n "Build childA ... "
	@$(CC) $(TASKA_OBJ) $(FLAGS) -o childA
	@echo "Complete"

childB: $(TASKB_OBJ)
	@echo -n "Build childB ... "
	@$(CC) $(TASKB_OBJ) $(FLAGS) -o childB
	@echo "Complete"

clean:
	@echo "Clean objects and binaries..."
	@rm -rf $(TARGET) $(ALL_OBJ)

all: $(ALL_OBJ) $(TARGET)

rebuild: clean all

.PHONY: clean all rebuild
