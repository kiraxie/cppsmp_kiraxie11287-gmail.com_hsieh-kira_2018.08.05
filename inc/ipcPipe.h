/**************************************************************************************************
 *  ipcPipe.h
 *************************************************************************************************/
#ifndef __IPC_PIPE_H__
#define __IPC_PIPE_H__

#include <vector>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ipcSem.h"
#include "ipcPacket.h"

class ipcPipe : public ipcSem
{
    public:
        ipcPipe(int flag);
        ~ipcPipe();

        ipcPacketCmd        ipcRecv(std::vector<unsigned int> *_v);
        void                ipcSend(ipcPacketCmd type, std::vector<unsigned int> *_v);

    private:
        int                 _pipeFd;

        int _read(void *buff, int len) {
#if 0
            struct timeval  t;
            fd_set          read_fds;
            int             ret = 0;
            int             done = 0;
            char            *p = (char *)buff;

            FD_ZERO(&read_fds);
            FD_SET(_pipeFd, &read_fds);
            t.tv_sec = 1;
            t.tv_usec = 0;

            do {
                if((ret = select(_pipeFd + 1, &read_fds, NULL, NULL, &t)) > 0) {
                    ret = read(_pipeFd, p + done, len - done);
                    if(ret < 0) {
                        return ret;
                    } else {
                        done += ret;
                    }
                    t.tv_sec = 1;
                    t.tv_usec = 0;
                }
            } while(done < len && ret > 0);

            return (done == len) ? done : ret;
#else
            int                 ret = 0;
            int                 done = 0;
            char                *p = NULL;

            p = (char *)buff;
            done = 0;

            while(done < len) {
                ret = read(_pipeFd, p + done, len - done);
                if(ret < 0) {
                    return ret;
                } else if(ret == 0) {
                    return done;
                } else {
                    done += ret;
                }
            }

            return done;
#endif
        }

        int _write(void *buff, int len) {
            int                 ret = 0;
            int                 done = 0;
            char                *p = NULL;

            p = (char *)buff;
            done = 0;

            while(done < len) {
                ret = write(_pipeFd, p + done, len - done);
                if(ret < 0) {
                    return ret;
                } else if(ret == 0) {
                    continue;
                } else {
                    done += ret;
                }
            }

            return done;
        }
};

#endif /*   __IPC_PIPE_H__  */
