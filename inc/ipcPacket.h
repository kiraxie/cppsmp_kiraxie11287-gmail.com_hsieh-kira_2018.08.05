/**************************************************************************************************
 *  ipcPacket.h
 *************************************************************************************************/
#ifndef __IPC_PACKET_H__
#define __IPC_PACKET_H__

typedef enum {
    IPC_PACKET_CMD_DATA = 0,
    IPC_PACKET_CMD_EXIT
} ipcPacketCmd;

typedef struct {
    ipcPacketCmd        type;
    int                 len;
} ipcPacketHdr;

#endif /*   __IPC_PACKET_H__  */
