/**************************************************************************************************
 *  task.h
 *************************************************************************************************/
#ifndef __TASK_H__
#define __TASK_H__
#include <vector>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "global.h"
#include "ipcShm.h"
#include "ipcPipe.h"

class task
{
    public:
        void                printData_(std::vector<unsigned int> *_v);
        void                printSortData_(std::vector<unsigned int> *_v);
};

class taskA : public ipcPipe, public task
{
    public:
        taskA(): ipcPipe(O_RDONLY) {
            ;
        }

        ipcPacketCmd        recvData(void);
        void                printData();
        void                printSortData();
        double              calculateMedian(void);

    private:
        std::vector<unsigned int>    _v;
};

class taskB : public ipcShm, public task
{
    public:
        ipcPacketCmd        recvData(void);
        void                printData();
        void                printSortData();
        double              calculateGeometricMean(void);

    private:
        std::vector<unsigned int>    _v;
};


#endif /*   __TASK_H__  */
