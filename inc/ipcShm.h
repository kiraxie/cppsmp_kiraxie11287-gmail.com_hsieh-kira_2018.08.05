/**************************************************************************************************
 *  ipcShm.h
 *************************************************************************************************/
#ifndef __IPC_SHM_H__
#define __IPC_SHM_H__

#include <vector>
#include <string.h>

#include "ipcSem.h"
#include "ipcPacket.h"

#define SHM_MAX_MEM_SIZE                    (4096)

class ipcShm : public ipcSem
{
    public:
        ipcShm();
        ~ipcShm();

        ipcPacketCmd        ipcRecv(std::vector<unsigned int> *_v);
        void                ipcSend(ipcPacketCmd type, std::vector<unsigned int> *_v);
    private:
        int                 _shmId;
        void*               _shmAddr;

        void _readHdr(ipcPacketHdr *hdr) {
            take(IPC_SEM_SLAVE);
            memcpy(hdr, _shmAddr, sizeof(ipcPacketHdr));
            give(IPC_SEM_MASTER);
        }

        void _writeHdr(ipcPacketHdr *hdr) {
            take(IPC_SEM_MASTER);
            memcpy(_shmAddr, hdr, sizeof(ipcPacketHdr));
            give(IPC_SEM_SLAVE);
        }

        void _readData(std::vector<unsigned int> *__v, int o, int l) {
            int            *arr = (int *)_shmAddr;

            take(IPC_SEM_SLAVE);
            __v->insert(__v->begin() + o , arr, arr + l);
            give(IPC_SEM_MASTER);
        }

        void _writeData(std::vector<unsigned int> *__v, int o, int l) {
            int            *arr = (int *)_shmAddr;

            take(IPC_SEM_MASTER);
            for(int i = 0; i < l; ++i) {
                arr[i] = __v->at(o + i);
            }
            give(IPC_SEM_SLAVE);
        }
};


#endif /*   __IPC_SHM_H__  */
