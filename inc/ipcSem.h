/**************************************************************************************************
 *  ipcSem.h
 *************************************************************************************************/
#ifndef __IPC_SEM_H__
#define __IPC_SEM_H__

typedef enum {
    IPC_SEM_MASTER = 0,
    IPC_SEM_SLAVE,
    IPC_SEM_TASK_A,
    IPC_SEM_TASK_B,
    IPC_SEM_MAX
} ipcFuncSemOpNum;

class ipcSem
{
    public:
        ipcSem();
        ~ipcSem();

        int             take(unsigned short num);
        int             give(unsigned short num);
        int             watch(unsigned short num);
        void            master(void);
    private:
        bool            _master;
        int             _semID;
};

#endif /*   __IPC_SEM_H__  */
