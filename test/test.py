#!/usr/bin/python

import os, sys, subprocess

def averageNum(num):
    nsum = 0
    for i in range(len(num)):
        nsum += num[i]
    return round(float(nsum) / len(num), 7)

def medianNum(num):
    listnum = [num[i] for i in range(len(num))]
    listnum.sort()
    lnum = len(num)
    if lnum % 2 == 1:
        i = int((lnum + 1) / 2)-1
        return round(float(listnum[i]), 7)
    else:
        i = int(lnum / 2)-1
        return round(float(listnum[i] + listnum[i + 1]) / 2, 7)

def getDataArrByStr(source, substr):
    position = source.find(substr)
    dataArr = map(int, source[position + len(substr) : source.find("\n", position)].strip().split(" "))
    return dataArr

def getDoubleByStr(source, substr):
    position = source.find(substr)
    val = source[position + len(substr) : source.find("\n", position)].strip()
    return float(val)

def main():
    argv = sys.argv
    argc = len(argv)

    if argc != 2:
        print 'Usage: /path/test.py /path/mainParent'
        sys.exit()

    #   Normal Case
    errCnt = 0
    totalCnt = 100

    for i in xrange(1, totalCnt + 1):
        p = subprocess.Popen(argv[1], shell = True, stdout = subprocess.PIPE, stderr = subprocess.PIPE, stdin = subprocess.PIPE)
        stdout, stderr = p.communicate(input = str(i))
        err = False

        dataArr = getDataArrByStr(stdout, "random integers: ")
        pipeDataArr = getDataArrByStr(stdout, "From Pipe: ")
        shmDataArr = getDataArrByStr(stdout, "From Shared Memory: ")
        sortDataArr = getDataArrByStr(stdout, "Sorted Sequence: ")
        mean = getDoubleByStr(stdout, "Geometric Mean: ")
        med = getDoubleByStr(stdout, "Median: ")

        if not(set(dataArr) & set(pipeDataArr)):
            print "The PIPE array not match"
            err = True

        if not(set(dataArr) & set(shmDataArr)):
            print "The share memory array not match"
            err = True

        if not(set(sorted(dataArr)) & set(sortDataArr)):
            print "The sort array not match"
            err = True

        calMean = averageNum(dataArr)
        if calMean != float(mean):
            print "The geometric mean not match. cal: " + str(calMean) + ', org: ' + str(mean)
            err = True

        calMed = medianNum(dataArr)
        if calMed != float(med):
            print "The median number not match. cal: " + str(calMed) + ', org: ' + str(med)
            err = True

        if err:
            errCnt = errCnt + 1

        p.wait()

    #   Error Case
    errInput = ['-1', '-10', '5.5', '80.555']
    for val in errInput:
        p = subprocess.Popen(argv[1], shell = True, stdout = subprocess.PIPE, stderr = subprocess.PIPE, stdin = subprocess.PIPE)
        stdout, stderr = p.communicate(input = val)
        err = False
        if stdout.find('please try again.') == -1:
            errCnt = errCnt +1
            print val

    if errCnt == 0:
        print 'Test Pass!'
    else:
        print "Total Count: " + str(totalCnt + len(errInput)) + ", Error Count: " + str(errCnt)

# Main Process
if __name__ == '__main__':
    main()
